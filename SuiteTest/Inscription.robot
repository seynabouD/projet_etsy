*** Settings ***

Resource    ../Keywords/Commun.resource
Resource    ../Keywords/AccueilKeywords.resource
Resource    ../Keywords/PageProduit.resource
Resource    ../SuiteTest/Config.resource
Resource     ../Keywords/Inscription.resource
Test Setup    OuvrirBrowser
#Test Teardown    FermerBrowser
#Test Teardown    FermerBrowser
#Test Setup avant chaque test
*** Test Cases ***
Acceder à lapage d'accueil
    Naviguer sur le site de vente

 Inscription via formulaire reussie avec mot de passe et adresse correcte
    Naviguer sur le site de vente
    Clique sur le bouton se connecter pour s'incrire et/ou se connecter
    Inscription via le formulaire    ${mon_email_correct}      ${mon_prenom}    ${mon_password}

 Inscription via formulaire avec adresse mail incorrecte et mot de passe correcte
     Naviguer sur le site de vente
     Clique sur le bouton se connecter pour s'incrire et/ou se connecter
     Inscription via le formulaire    ${mon_email_incorrect}     ${mon_prenom}    ${mon_password}
     Verifier le message d'erreur de l'adresse mail
   
Inscription via formulaire avec une adresse mail correcte et mot de passe incorrect
    Naviguer sur le site de vente
    Clique sur le bouton se connecter pour s'incrire et/ou se connecter
    Inscription via le formulaire    ${mon_email_correct}      ${mon_prenom}   ${mon_password_incorrect} 
    Verifier le message d'erreur du mot de passe    ${error_test_message} 

Inscription via google
    Naviguer sur le site de vente
    Clique sur le bouton se connecter pour s'incrire et/ou se connecter
    Cliquer sur le bouton continuer google
    Choisir le compte
    Remplir le formulaire google

Inscription via Facebook

   



    
