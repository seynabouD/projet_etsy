*** Settings ***
Resource    ../Keywords/Commun.resource
Resource    ../Keywords/AccueilKeywords.resource
Resource    ../SuiteTest/Config.resource
Resource     ../Keywords/Inscription.resource
Resource    ../Keywords/Connexion.resource
Test Setup    OuvrirBrowser
#Test Teardown    FermerBrowser
#Test Teardown    FermerBrowser
#Test Setup avant chaque test
*** Test Cases ***

Connexion via le formulaire avec mot de passe et adresse mail correcte
    Naviguer sur le site de vente
    Clique sur le bouton se connecter pour s'incrire et/ou se connecter
    Connexion via le formulaire   ${mon_email_correct}   ${mon_password} 
Connexion via le formulaire avec mot de passe incorrect et adresse mail correcte
    Naviguer sur le site de vente
    Clique sur le bouton se connecter pour s'incrire et/ou se connecter
    Connexion via le formulaire    ${mon_email_correct}    ${mon_password_incorrect}   
    Verifier le message d'erreur de la connexion : mot de passe incorrect
Connexion via le formulaire avec mot de passe correct et adresse mail incorrecte
    Naviguer sur le site de vente
    Clique sur le bouton se connecter pour s'incrire et/ou se connecter
    Connexion via le formulaire    ${mon_email_incorrect}   ${mon_password}
    Verifier le message d'erreur de la connexion : adresse mail incorrect
 


